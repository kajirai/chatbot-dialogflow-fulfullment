  /**
   * Enum for Dialogflow v1 text message object https://dialogflow.com/docs/reference/agent/message-objects
   */
  const v1MessageObjectCard = 1;
  
  /**
   * Class representing a card response
   * @extends RichResponse
   */
//   class Card extends RichResponse {
class KonCard {
    constructor(card) {
      //super();
      this.templateId =  10;
      // if (card === undefined || (typeof card === 'object' && !card.title)) {
      //   throw new Error('title string required by Card constructor');
      // }
      if (typeof card === 'string') {
        this.title = card;
      } else if (typeof card === 'object') {
        this.message            = card.message;
        this.platform           = card.platform;

        this.contentType        = card.contentType;

        this.title              = card.title;
        this.subtitle           = card.subtitle;
        //HEADER
        this.overlayText        = card.overlayText;
        this.imgSrc             = card.imgSrc;

        //CARD INFORMATION
        this.description        = card.description;
        this.titleExt           = card.titleExt;

        //CARD FOOTER POSSIBLEITIES | LINKS, BUTTONS , SUBMIT, QUICKREPLY
        this.footer             = card.footer  //OBJECT FOR NOW
      }
    }
  
    setTitle(title) {
      if (typeof title !== 'string') {
        throw new Error('setText requires a string of the text');
      }
      this.title = title;
      return this;
    }
  
    setText(text) {
      if (typeof text !== 'string') {
        throw new Error('setText requires a string of the text');
      }
      this.text = text;
      return this;
    }
  
    /**
     * Set the image for a Card
     *
     * @example
     * let card = new KonCard();
     * card.setImage('https://assistant.google.com/static/images/molecule/Molecule-Formation-stop.png');
     *
     * @param {string} imageUrl
     * @return {Card}
     */
    setImage(imageUrl) {
      if (typeof imageUrl !== 'string') {
        throw new Error('setImage requires a string of the image URL');
      }
      this.imageUrl = imageUrl;
      return this;
    }
  
    setButton(button) {
      if ((!button.text && button.url) || (!button.url && button.text)) {
        throw new Error(
          `card button requires button title and url. \
  \nUsage: setButton({text: \'button text\', url: \'http://yoururlhere.com\'}`
        );
      }
      this.buttonText = button.text;
      this.buttonUrl = button.url;
      return this;
    }

    getResponseObject_() {
  
      let response;
      response = {};
      response.message                        = this.message;
      response.platform                       = this.platform;
      // this.response.metadata                  = {};
      let metadata                            = {};
      metadata.contentType                    = this.contentType;
      metadata.templateId                     = this.templateId;

      // this.response.metadata.contentType      = this.contentType;
      // this.response.metadata.templateId       = this.templateId;
      let payload                             = {};
      payload.title                           = this.title;
      payload.subtitle                        = this.subtitle;

      // this.response.metadata.payload          =  [];
      // this.response.metadata.payload.title    = this.title;
      // this.response.metadata.payload.subtitle    = this.subtitle;

      // this.response.metadata.payload.subtitle    = this.subtitle;
      // this.response.metadata.payload.subtitle    = this.subtitle;

      let header = {};
      header.overlayText                      = this.overlayText;
      header.imgSrc                           = this.imgSrc;

      payload.header                          = header;

      payload.description                     = this.description;
      payload.titleExt                        = this.titleExt;
      
      let footer = this.footer;
      //console.log("ABOUT TO RUN...: " + JSON.stringify(footer));
      for (var key in footer) {
          //console.log("Checking keys: " + JSON.stringify(footer));
          if (footer.hasOwnProperty(key)) {
              //console.log(key + " -> " + footer[key]);
              payload[key] = footer[key];
          }
      }
      metadata.payload = [payload];
      response.metadata = metadata;

      return response;
    }
}
  module.exports = KonCard;
  