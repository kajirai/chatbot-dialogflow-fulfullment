"use strict";
// module.exports = function helper() {
//     this.isNullOrEmpty = function(str) {
//         if (null == str) return true;
//         str = this.trim(str);
//         return str.length == 0;
//     }
    
//     this.trim = function(str, chars) {
//         return this.ltrim(this.rtrim(str, chars), chars);
//     }
 
//     this.ltrim = function(str, chars) {
//         chars = chars || "\\s";
//         return str.replace(new RegExp("^[" + chars + "]+", "g"), "");
//     }
    
//     this.rtrim = function (str, chars) {
//         chars = chars || "\\s";
//         return str.replace(new RegExp("[" + chars + "]+$", "g"), "");
//     }
//     this.return2br = function (str) {
//         if (this.isNullOrEmpty(str)) return "";
//         return str.replace(/(\r\n|\r|\n)/g, "<br />");
//     }
//     this.getTime = ((d) => {
//         var date = new Date();
//         var asiaTime = new Date().toLocaleString("en-US", {timeZone: "Asia/Shanghai"});
//         asiaTime = new Date(asiaTime);
//         console.log('Asia time: '+asiaTime.toLocaleString());
//         var hour = date.getHours();
//         hour = (hour < 10 ? "0" : "") + hour;
    
//         var min  = date.getMinutes();
//         min = (min < 10 ? "0" : "") + min;
    
//         var sec  = date.getSeconds();
//         sec = (sec < 10 ? "0" : "") + sec;
    
//         var year = date.getFullYear();
    
//         var month = date.getMonth() + 1;
//         month = (month < 10 ? "0" : "") + month;
    
//         var day  = date.getDate();
//         day = (day < 10 ? "0" : "") + day;
    
//         return asiaTime.toLocaleString();
//     });

//     this.getDate = ((d) => {
//         let date = ("0" + curr_dt.getDate()).slice(-2);
//         let month = ("0" + (curr_dt.getMonth() + 1)).slice(-2);
//         let year = curr_dt.getFullYear();
//         return year + "-" + month + "-" + date;
//     });


// }

// //var StringHelper = new stringHelper();

var helper = {
    isNullOrEmpty: function(str) {
        if (null == str) return true;
        str = this.trim(str);
        return str.length == 0;
    },
    
    trim: function(str, chars) {
        return this.ltrim(this.rtrim(str, chars), chars);
    },
 
    ltrim: function(str, chars) {
        chars = chars || "\\s";
        return str.replace(new RegExp("^[" + chars + "]+", "g"), "");
    },
    
    rtrim: function (str, chars) {
        chars = chars || "\\s";
        return str.replace(new RegExp("[" + chars + "]+$", "g"), "");
    },
    return2br: function (str) {
        if (this.isNullOrEmpty(str)) return "";
        return str.replace(/(\r\n|\r|\n)/g, "<br />");
    },

    getDate: (d)=> {
        let date = ("0" + d.getDate()).slice(-2);
        let month = ("0" + (d.getMonth() + 1)).slice(-2);
        let year = d.getFullYear();
        return year + "-" + month + "-" + date;
    },

    getTime: (d)=> {
        console.log('GETTING TIMEEEEEEEEEEEEEEEEE');
        //var date = new Date();
        //var asiaTime = new Date().toLocaleString("en-US", {timeZone: "Asia/Shanghai"});
        d = new Date(d);
        console.log('Asia time: '+d.toLocaleString());
        var hour = d.getHours();
        hour = (hour < 10 ? "0" : "") + hour;
    
        var min  = d.getMinutes();
        min = (min < 10 ? "0" : "") + min;
    
        var sec  = d.getSeconds();
        sec = (sec < 10 ? "0" : "") + sec;
    
        var year = d.getFullYear();
    
        var month = d.getMonth() + 1;
        month = (month < 10 ? "0" : "") + month;
    
        var day  = d.getDate();
        day = (day < 10 ? "0" : "") + day;
        return hour + ':' + min + ':' + sec; 
        // return asiaTime.toLocaleString();
    }
}

//MyStaticClass.someMethod(); // Doing someMethod

module.exports = helper;