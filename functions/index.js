// See https://github.com/dialogflow/dialogflow-fulfillment-nodejs
// for Dialogflow fulfillment library docs, samples, and to report issues
'use strict';
 
const functions = require('firebase-functions');
const {WebhookClient} = require('dialogflow-fulfillment');
//const {Card, Suggestion, PayLoad} = require('dialogflow-fulfillment');
const {Text, Card, Image, Suggestion, Payload} = require('dialogflow-fulfillment'); 

process.env.DEBUG = 'dialogflow:debug'; // enables lib debugging statements
 
const helper = require('./lib/helper');
const KonCard = require('./lib/Kommunicate/KonCard');
const Kommunicate = require("./lib/Kommunicate/Kommunicate"); 

const admin = require('firebase-admin');
// admin.initializeApp();
admin.initializeApp({
  credential: admin.credential.applicationDefault(),
  databaseURL: 'ws://invisalign-vxdvnt.firebaseio.com/'
})

exports.dialogflowFirebaseFulfillment_2 = functions.https.onRequest((request, response) => {
  const agent = new WebhookClient({ request, response });
  console.log('Dialogflow Request headers: ' + JSON.stringify(request.headers));
  console.log('Dialogflow Request body: ' + JSON.stringify(request.body));

  function welcome(agent) {
    agent.add(`Welcome to my agent!`);
  }
  //const {helper} = require('./lib/helper');
	function fallback(agent) {
		agent.add(`I didn't understand`);
		agent.add(`I'm sorry, can you try again?`);
	}
  //HELPER FUNCTIONS//
  //global.helper = helper;
  function demo_time(agent){
    //let curr_dt = new Date();
    var asiaTime = new Date().toLocaleString("en-US", {timeZone: "Asia/Shanghai"});
    agent.add(`Yes Certainly...`);
    //let helper = new helper();
    agent.add(`The time now is: ` + helper.getTime(asiaTime));
  }
  
	function who_dentistry(agent) {
    //agent.add('Yes Certainly...');
    //let data = {'message': '', 'platform': "kommunicate", 'contentType': "300", 'templateId': "10"};
    const kommunicate = new Kommunicate();
    //console.log('getResponseObject_ kommunicate.add(konCard) = ', JSON.stringify(kommunicate.testLog_()));
    //  //kommunicate.testLog_();
    const konCard = new KonCard({
               'message'       : 'hello message',
               'platform'      : 'kommunicate',
               'contentType'   : '300',
               'title'         : 'This is the title',
               'subtitle'      : 'This is the subtitle',
               'overlayText'   : 'HERE IS THE OVERLAY TEXTS',
               'imgSrc'        : 'https://blog.sesamehub.com/ziehmer-richard/files/2019/06/Invisalign.jpg',
               'description'   : 'THe card Description',
               'titleExt'      : 'Title Extention',
               'footer'        : {
                                   'buttons' : [
                                     {
                                       "name": "More about It...",
                                       "action": {
                                         "type": "link",
                                         "payload": {
                                           "url": "https://en.wikipedia.org/wiki/Dentistry"
                                         }
                                       }
                                     }
                                   ]
                                 }
             });
    // console.log('getResponseObject_ konCard = ', JSON.stringify(konCard.getResponseObject_()));
    let kon_Payload = kommunicate.add(konCard);
    console.log('kon_Payload +++++++++++++++++++++++ = ', JSON.stringify(kon_Payload));
    agent.add(new Payload("PLATFORM_UNSPECIFIED", kon_Payload));
    // //MAKE PAYLOAD//


    // agent.add(new Card({
    //   //       title: `Title: this is a card title`,
    //   //       imageUrl: 'https://developers.google.com/actions/images/badges/XPM_BADGING_GoogleAssistant_VER.png',
    //   //       text: `This is the body text of a card.  You can even use line\n  breaks and emoji! 💁`,
    //   //       buttonText: 'This is a button',
    //   //       buttonUrl: 'https://assistant.google.com/'
    //   //     })
    //   //   );
    // }));

    //kon.

  // console.log("WORKING ON KOMMUNICATE PAYLOAD>>>>");
  // agent.add('Yes Certainly...');
  // agent.add(new Payload("PLATFORM_UNSPECIFIED", [{
  //     "message": "Certainly, here is more about 'Dentistry'...",
  //     "platform": "kommunicate",
  //     "metadata": {
	// 	"contentType": "300",
	// 	"templateId": "10",
  //       "payload": [
  //         {
  //       "title": "Dentistry",
  //       "subtitle": "What is it about?",
  //       "header": {
  //         "overlayText": "Fresh!",
  //         "imgSrc": "https://blog.sesamehub.com/ziehmer-richard/files/2019/06/Invisalign.jpg"
  //       },
  //       "description": "Dentistry, also known as Dental and Oral Medicine, is a branch of medicine that consists of the study, diagnosis, prevention, and treatment of diseases, disorders, and conditions of the oral cavity, commonly in the dentition but also the oral mucosa, and of adjacent and related structures and tissues, particularly in the maxillofacial (jaw and facial) area.[1] Although primarily associated with teeth among the general public, the field of dentistry or dental medicine is not limited to teeth but includes other aspects of the craniofacial complex including the temporomandibular joint and other supporting, muscular, lymphatic, nervous, vascular, and anatomical structures.",
  //       "titleExt": "It's all about healthy Teeth!",
  //       "buttons": [
  //         {
  //           "name": "More about It...",
  //           "action": {
  //             "type": "link",
  //             "payload": {
  //               "url": "https://en.wikipedia.org/wiki/Dentistry"
  //             }
  //           }
  //         }
  //       ]
  //     }]
	// }
	// }]));
      
	}
  
  function appnt_book_fix_type(agent){
    console.log('appnt_book_fix_type_agent.parameters[\'bite_type\']: ' + agent.parameters['bite_type']);
    let fix_type = agent.parameters['bite_type'];
    fix_type = fix_type.toLowerCase();
    console.log('appnt_book_fix_type_agent.parameters[\'bite_type\'] To Lower: ' + fix_type);
    let bite_types = ['crossbite', 'underbite', 'overbite'];
    // fix_type = fix_type.toLowerCase();
    if (bite_types.indexOf(fix_type) > -1) {
      // agent.add(`Thank you, so your bite type is:` + fix_type);
      agent.add(new Payload("PLATFORM_UNSPECIFIED", [      {
        "message":`Thank you, so your bite type is:` + fix_type + `So Are you?`,
        "platform": "kommunicate",
        "metadata": {
            "contentType": "300",
            "templateId": "6",
            "payload": [{
                "title": "I am an Adult",
                "message": "Hi Adult!"
            }, {
                "title": "I am a Teen",
                "message": "Cool Teen!",
                "replyMetadata": {
                    "KM_CHAT_CONTEXT": {
                        "buttonClicked": true
                    }
                }
            }]
        }
      }]
      ));
    } else {
      agent.add(`Sorry but your inputted types does not match please try again, thank you!`);
    }
  }
  function appnt_book_fix_type_fallback(agent){
    console.log('appnt_book_fix_type_fallback.agent.query: ' + agent.query);
    let fix_type = agent.query;
    fix_type = fix_type.toLowerCase();
    console.log('appnt_book_fix_type_fallback.fix_type To Lower: ' + fix_type);
    let bite_types = ['crossbite', 'underbite', 'overbite'];
    // fix_type = fix_type.toLowerCase();
    if (bite_types.indexOf(fix_type) > -1) {
      agent.add(`Thank you, so your bite type is:` + fix_type);
    } else {
      agent.add(`Sorry but your inputted types does not match please try again, thank you!`);
    }
  }

  function appnt_book_detail(agent){
    console.log('appnt_book_detail: ' + agent.query);
    let name = agent.parameters['name'];
    let person = agent.parameters['person'];
    let status = agent.parameters['status'];
    let are_you = agent.parameters['are_you'];
    let bite_type = agent.parameters['bite_type'];
    let email = agent.parameters['email'];
    let phone_number = agent.parameters['phone-number'];
    let appt_day = agent.parameters['day'];
    
    //let fix_type = agent.query;

    console.log('appnt_book_detail person: ' + person);
    let bite_types = ['crossbite', 'underbite', 'overbite'];
    
    return admin.database().ref('bookings').push({
      person: person, status: status, are_you: are_you, bite_type: bite_type, email: email, phone_number: phone_number, appt_day: appt_day
      }).then((snapshot) =>
    {
      console.log('appnt_book_detail database write successful: ' + snapshot.ref.toString());
    });

  }



  function my_name_is(agent){
    const city = agent.parameters['geo-city'];
  }
  
  function goodbye(){
    const name = agent.parameters['session-name'];
    var s = name;
    let newName = ``;
    for (var i = 0; i < s.length; i++) {
      console.log(s.charAt(i));
      newName += `-` + s.charAt(i);
    }
    agent.add(`GoodBye: ` + newName + `FROM dialogflowFirebaseFulfillment_2`);
  }
  
  // Run the proper function handler based on the matched Dialogflow intent name
  let intentMap = new Map();
  intentMap.set('Default Welcome Intent', welcome);
  intentMap.set('Default Fallback Intent', fallback);
  // intentMap.set('your intent name here', yourFunctionHandler);
  // intentMap.set('your intent name here', googleAssistantHandler);
  intentMap.set('!demo_time', demo_time);
  intentMap.set('!who_dentistry', who_dentistry);
  intentMap.set('!goodbye', goodbye);
  intentMap.set('!appnt_book - fix_type', appnt_book_fix_type);
  intentMap.set('!appnt_book - fix_type - fallback', appnt_book_fix_type_fallback);
  intentMap.set('!appnt_book-fallback', appnt_book_fix_type_fallback);
  intentMap.set('!appnt_book - fallback', appnt_book_fix_type_fallback);
  intentMap.set('!appnt_book - fix_type - are_you - status - details', appnt_book_detail);
  agent.handleRequest(intentMap);
});

// /******************
//  * TEST
//  * SECTION
//  */

// // const Kommunicate = require("./lib/Kommunicate/Kommunicate"); 
// ////const Card = require("./lib/Kommunicate/Kommunicate"); 
// //  //import {Card as KonCard} from './lib/Kommunicate/Kommunicate.js';
// // const KonCard = require('./lib/Kommunicate/Kommunicate');
// const kommunicate = new Kommunicate();
// //console.log('getResponseObject_ kommunicate.add(konCard) = ', JSON.stringify(kommunicate.testLog_()));
// //  //kommunicate.testLog_();
// const konCard = new KonCard({
//            'message'       : 'hello message',
//            'platform'      : 'kommunicate',

//            'contentType'   : '300',

//            'title'         : 'This is the title',
//            'subtitle'      : 'This is the subtitle',
//            //HEADER
//            'overlayText'   : 'HERE IS THE OVERLAY TEXTS',
//            'imgSrc'        : 'https://blog.sesamehub.com/ziehmer-richard/files/2019/06/Invisalign.jpg',

//            //CARD INFORMATION
//            'description'   : 'THe card Description',
//            'titleExt'      : 'Title Extention',
//            'footer'        : {
//                                'buttons' : [
//                                  {
//                                    "name": "More about It...",
//                                    "action": {
//                                      "type": "link",
//                                      "payload": {
//                                        "url": "https://en.wikipedia.org/wiki/Dentistry"
//                                      }
//                                    }
//                                  }
//                                ]
//                              }
//          });
// // console.log('getResponseObject_ konCard = ', JSON.stringify(konCard.getResponseObject_()));
// console.log('getResponseObject_ konCard = ', JSON.stringify(kommunicate.add(konCard)));

// // kommunicate.add(konCard);


// //  const kommunicate = new Kommunicate();
// //  console.log('Object property = ', kommunicate.val);
// //  kommunicate.pubMethod(28);

// // import Kommunicate from './lib/Kommunicate/Kommunicate';
// //import pkg from “./pkg”
// //const Kommunicate = require('Kommunicate');
// //const Kon_Card = require("./lib/Kommunicate/kommunicate");

// //global.helper = helper;




